package com.moravskiyandriy.pageobjects.implementations;

import com.moravskiyandriy.pageobjects.AbstractPageObject;
import com.moravskiyandriy.utils.Waiter;
import com.moravskiyandriy.utils.listeners.TestListener;
import com.moravskiyandriy.webelement.implementations.Button;
import com.moravskiyandriy.webelement.implementations.InputTextField;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.Listeners;

@Listeners({TestListener.class})
public class AuthenticationPage extends AbstractPageObject {
    private static final Logger LOGGER = LogManager.getLogger(AuthenticationPage.class);
    @FindBy(css = "input[type='email']")
    private InputTextField loginField;
    @FindBy(css = "input[name='password']")
    private InputTextField passwordField;
    @FindBy(id = "identifierNext")
    private Button submitLoginButton;
    @FindBy(id = "passwordNext")
    private Button submitPasswordButton;

    @Step("Going to gmail page.")
    public void goToGmailPage() {
        LOGGER.info("going to Gmail Page");
        driver.get("https://mail.google.com/mail");
    }

    @Step("Filling login field with {0}.")
    public void fillLoginField(String login) {
        LOGGER.info("filling Login Field");
        loginField.sendKeys(login);
    }

    @Step("Submitting login.")
    public void submitLogin() {
        LOGGER.info("submitting Login");
        Waiter.waitForElementToBeClickable(submitLoginButton, Waiter.SMALL_WAIT);
        submitLoginButton.click();
    }

    @Step("Filling password field with {0}.")
    public void fillPasswordField(String password) {
        LOGGER.info("filling Password Field");
        passwordField.sendKeys(password);
    }

    @Step("Submitting password.")
    public void submitPassword() {
        LOGGER.info("submitting Password");
        Waiter.waitForElementToBeClickable(submitPasswordButton, Waiter.SMALL_WAIT);
        submitPasswordButton.click();
    }
}
