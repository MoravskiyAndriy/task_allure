package com.moravskiyandriy.webelement.implementations;

import com.moravskiyandriy.utils.Waiter;
import com.moravskiyandriy.webelement.BasicWebElement;
import org.openqa.selenium.WebElement;

public class CheckBox extends BasicWebElement {
    public CheckBox(WebElement WEBElement) {
        super(WEBElement);
    }

    public void setCheckBoxSelected() {
        Waiter.waitForElementToBeClickable(webElement, Waiter.LONG_WAIT);
        if (!webElement.isSelected()) {
            webElement.click();
        }
    }

    public boolean isSelected() {
        Waiter.waitForElementToBeClickable(webElement, Waiter.SMALL_WAIT);
        return webElement.isSelected();
    }
}
