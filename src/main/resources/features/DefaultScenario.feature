Feature: Logging into Gmail, deleting Letters, undoing action

  Scenario Outline: Checking behavior
    Given User with login '<LoginString>', password '<PasswordString>' is logged into Gmail
    And base letters' quantity
    When deleting some letters and undoing action
    And getting letters' quantity
    Then checking if letters' quantity is unchanged

    Examples:
    | LoginString                 | PasswordString    |
    |NickScorpi@gmail.com         |sc0rp198           |
    |SeleniumWebzxcTest1@gmail.com|seleniumwebzxctest1|
    |SeleniumWebzxcTest2@gmail.com|seleniumwebzxctest2|
    |SeleniumWebzxcTest3@gmail.com|ZzXxCctest333      |
    |SeleniumWebzxcTest4@gmail.com|ZzXxCctest444      |