package com.moravskiyandriy.botest;

import com.moravskiyandriy.businessobjects.AuthenticationBO;
import com.moravskiyandriy.businessobjects.GmailBO;
import com.moravskiyandriy.dataparcer.CSVDataParser;
import com.moravskiyandriy.fortest.BaseTest;
import com.moravskiyandriy.utils.DriverManager;
import com.moravskiyandriy.utils.listeners.TestListener;
import io.qameta.allure.Description;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITest;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Optional;
import java.util.Properties;

import static org.testng.Assert.assertEquals;

@Listeners({TestListener.class})
public class DeleteLettersAndReverseActionTest extends BaseTest implements ITest{
    private static final String DATAFILE_PATH = "./src/main/resources/UserData.csv";
    private static final Logger LOGGER = LogManager.getLogger(DeleteLettersAndReverseActionTest.class);
    private static final int DEFAULT_QUANTITY_OF_LETTERS_TO_DELETE = 3;
    private static final int FINAL_QUANTITY_OF_LETTERS_TO_DELETE = Optional.ofNullable(getProperties()
            .getProperty("QUANTITY_OF_LETTERS_TO_DELETE"))
            .map(Integer::valueOf)
            .orElse(DEFAULT_QUANTITY_OF_LETTERS_TO_DELETE);

    private ThreadLocal<String> testName = new ThreadLocal<>();

    @BeforeMethod(alwaysRun = true)
    public void BeforeMethod(Method method, Object[] testData, ITestContext ctx){
        if (testData.length > 0) {
            testName.set(method.getName() + "_" + testData[0]);
            ctx.setAttribute("testName", testName.get());
        } else
            ctx.setAttribute("testName", method.getName());
    }

    @DataProvider(parallel = true)
    private Iterator<Object[]> users() {
        return CSVDataParser.readFile(DATAFILE_PATH);
    }

    @Test(dataProvider = "users")
    @Description("Test Description: Logging in and performing test actions.")
    void deleteLettersAndReverseAction(String accountAddress, String accountPassword) {
        int lettersQuantityBeforeDeletion;
        int lettersQuantityAfterDeletion;
        AuthenticationBO authenticationBO = new AuthenticationBO();
        GmailBO gmailBO = new GmailBO();
        authenticationBO.loginIntoGmail(accountAddress, accountPassword);
        lettersQuantityBeforeDeletion = gmailBO.goToIncomingAndCountLetters();
        gmailBO.chooseLettersDeleteUndo(FINAL_QUANTITY_OF_LETTERS_TO_DELETE);
        lettersQuantityAfterDeletion = gmailBO.goToIncomingAndCountLetters();
        assertEquals(lettersQuantityAfterDeletion, lettersQuantityBeforeDeletion);
    }

    @AfterMethod
    void quitDriver(ITestResult iTestResult, Method method){
        afterActions(iTestResult);
        DriverManager.quitDriver();
        DriverManager.setDriver(null);
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = DeleteLettersAndReverseActionTest.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            LOGGER.warn("NumberFormatException found.");
        } catch (IOException ex) {
            LOGGER.warn("IOException found.");
        }
        return prop;
    }

    @Override
    public String getTestName() {
        return testName.get();
    }
}
