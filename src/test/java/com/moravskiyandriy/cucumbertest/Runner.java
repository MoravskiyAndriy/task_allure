package com.moravskiyandriy.cucumbertest;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@Test
@CucumberOptions(features=".\\src\\main\\resources\\features\\DefaultScenario.feature",
        glue ="com.moravskiyandriy.cucumbertest.stepdefs")

public class Runner extends AbstractTestNGCucumberTests {
    @DataProvider(parallel = true)
    @Override
    public Object[][] scenarios() {
        return super.scenarios();
    }
}
