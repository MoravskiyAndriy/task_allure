package com.moravskiyandriy.cucumbertest.stepdefs;

import com.moravskiyandriy.botest.DeleteLettersAndReverseActionTest;
import com.moravskiyandriy.businessobjects.AuthenticationBO;
import com.moravskiyandriy.businessobjects.GmailBO;
import com.moravskiyandriy.utils.DriverManager;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;

import static org.testng.Assert.assertEquals;

public class StepDefs {
    private static final int DEFAULT_QUANTITY_OF_LETTERS_TO_DELETE = 3;
    private static final int FINAL_QUANTITY_OF_LETTERS_TO_DELETE = Optional.ofNullable(getProperties()
            .getProperty("QUANTITY_OF_LETTERS_TO_DELETE"))
            .map(Integer::valueOf)
            .orElse(DEFAULT_QUANTITY_OF_LETTERS_TO_DELETE);
    private static final Logger logger = LogManager.getLogger(DeleteLettersAndReverseActionTest.class);

    private GmailBO gmailBO = new GmailBO();
    private AuthenticationBO authenticationBO = new AuthenticationBO();

    private int baseQuantity=0;
    private int postActionQuantity=0;

    @Given("User with login {string}, password {string} is logged into Gmail")
    public void logInGmail(String login, String password) {
        authenticationBO.loginIntoGmail(login,password);
    }

    @And("base letters' quantity")
    public void getBaseLettersQuantity() {
        baseQuantity = gmailBO.goToIncomingAndCountLetters();
    }

    @When("deleting some letters and undoing action")
    public void SelectLettersDeleteUndo() {
        gmailBO.chooseLettersDeleteUndo(FINAL_QUANTITY_OF_LETTERS_TO_DELETE);
    }

    @And("getting letters' quantity")
    public void getNewLettersQuantity() {
        postActionQuantity = gmailBO.goToIncomingAndCountLetters();
    }

    @Then("checking if letters' quantity is unchanged")
    public void checking_if_letters_quantity_is_unchanged() {
        assertEquals(baseQuantity,postActionQuantity);
    }

    @After
    public void quitDriver(){
        DriverManager.quitDriver();
        DriverManager.setDriver(null);
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = DeleteLettersAndReverseActionTest.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
